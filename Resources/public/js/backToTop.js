/**
 * Back to top plugin.
 */
!function ($) {
    "use strict";
    
    var BackToTop = {
    	// Browser window scroll (in pixels) after which the "back to top" link is shown.
    	offset : 300,
    	// Browser window scroll (in pixels) after which the "back to top" link opacity is reduced.
		offset_opacity : 1200,
		// Duration of the top scrolling animation (in ms).
		scroll_top_duration : 700,
		build : function () {
			BackToTop.scroll();
			BackToTop.click();
		},
		// Hide or show the "back to top" link.
		scroll : function () {
			var offset = BackToTop.offset;
			var offset_opacity = BackToTop.offset_opacity;
			
			$(window).scroll(function(){
				if ($(this).scrollTop() > offset) {
					$("#top-link-block").removeClass("hidden");
				}
				else {
					$("#top-link-block").addClass("hidden")
				}
				
				if( $(this).scrollTop() > offset_opacity ) { 
					$("#top-link-block").addClass("hidden");
				}
			});
		},
		click : function () {
			var scroll_top_duration = BackToTop.scroll_top_duration;
			
			$("#top-link-block").on('click', function(e){
				e.preventDefault();
				$('body,html').animate({
					scrollTop: 0 ,
				 	}, scroll_top_duration
				);
			});
		}
    };

    $(document).ready(function () {
    	BackToTop.build();
    });
}(window.jQuery);