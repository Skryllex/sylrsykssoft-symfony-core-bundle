<?php
namespace SylrSyksSoftSymfony\Symfony\Bundle\Core\Tests\Builder;

use SylrSyksSoftSymfony\CoreBundle\Builder\Entity\EntityBuilder;
use SylrSyksSoftSymfony\CoreBundle\Builder\MasterTable\BuilderMasterTableBuilder;
use SylrSyksSoftSymfony\CoreBundle\Builder\MasterTable\MasterTableBuilderInterface;
use SylrSyksSoftSymfony\CoreBundle\Model\AbstractMasterTableModel;

final class TypeSeller extends AbstractMasterTableModel
{
    public function getClass() {
        return static::class;
    }
}

final class TypeSellerBuilder extends EntityBuilder implements MasterTableBuilderInterface
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \SylrSyksSoftSymfony\CoreBundle\Builder\MasterTableBuilderInterface::create()
     */
    public function create($code, $title, $description = NULL)
    {
        $this->object->setCode($code)
            ->setTitle($title)
            ->setDescription($description);
    }
}

final class MockTypeSeller
{

    public function get()
    {
        return array(
            'className' => TypeSeller::class,
            'code' => 'TRHJU-GG',
            'title' => 'Wholesaler',
            'description' => 'They are the ones who are responsible for buying certain products in bulk and reselling them at a higher price to retailers, who should buy it wholesale for profit of intermediation.'
        );
    }
}

final class MasterTableBuilderTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Definition test.
     */
    public function test()
    {
        $type = new TypeSeller();

        $this->assertSame('SylrSyksSoftSymfony\CoreBundle\Tests\Builder\TypeSeller', $type->getClass());
    }

    /**
     * Test build.
     */
    public function testBuild() {
        $mock_type = new MockTypeSeller();
        $type = $mock_type->get();
        $registry = $this->getMock('Doctrine\Common\Persistence\ManagerRegistry');
        $type_seller_builder = new TypeSellerBuilder($registry, $type['className']);

        $builder = new BuilderMasterTableBuilder($type_seller_builder);

        $builder->build($type['code'], $type['title'], $type['description']);
    }
}