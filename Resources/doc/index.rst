Getting Started With SylrSyksSoftSymfonyCoreBundle
==================================================

The ``SylrSyksSoftSymfonyCoreBundle`` provides a number of common features for other projects.

Prerequisites
-------------

This version of the bundle requires Symfony 2.8.

Installation
------------

Installation is a quick (I promise!) 3 step process:

1. Download SylrSyksSoftSymfonyCoreBundle using composer
2. Enable the Bundle
3. Configure the SylrSyksSoftSymfonyCoreBundle

Step 1: Download SylrSyksSoftSymfonyCoreBundle using composer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Require the bundle with composer:

.. code-block:: bash

    $ composer require sylrsykssoft-symfony/core-bundle "^1.0@dev"

Composer will install the bundle to your project's ``vendor/sylrsykssoft-symfony/core-bundle`` directory.

Step 2: Enable the bundle
~~~~~~~~~~~~~~~~~~~~~~~~~

Enable the bundle in the kernel::

    <?php
    // app/AppKernel.php

    public function registerBundles()
    {
        $bundles = array(
            // ...
            new SylrSyksSoftSymfony\CoreBundle\SylrSyksSoftSymfonyCoreBundle(),
            // ...
        );
    }

Step 3: Configure the SylrSyksSoftSymfonyCoreBundle
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Add the following configuration to your ``config.yml`` file according to which type
of datastore you are using.

.. configuration-block::

    .. code-block:: yaml

        # app/config/config.yml
        sylr_syks_soft_symfony_core:
            db_driver: orm # other valid value is 'mongodb'

    .. code-block:: xml

        <!-- app/config/config.xml -->

        <!-- other valid 'db-driver' values are 'mongodb' and 'couchdb' -->
        <sylr_syks_soft_symfony_core:config
            db-driver="orm"
        />

Only one configuration values are required to use the bundle:

* The type of datastore you are using (``orm``, ``mongodb``).

.. caution::

    When using one of the Doctrine implementation, you need either to use
    the ``auto_mapping`` option of the corresponding bundle (done by default
    for DoctrineBundle in the standard distribution) or to activate the mapping
    for SylrSyksSoftSymfonyCoreBundle otherwise the base mapping will be ignored.

Next Steps
~~~~~~~~~~

Now that you have completed the basic installation and configuration of the
SylrSyksSoftSymfonyCoreBundle, you are ready to learn about more advanced features and usages
of the bundle.

The following documents are available:

.. toctree::
    :maxdepth: 1

    configuration_reference
    adding_invitation_registration
