<?php
/**
 * @file
 * Common admin.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Admin;

use SylrSyksSoftSymfony\Symfony\Component\Enum\Role;
use Sonata\AdminBundle\Admin\AbstractAdmin as SonataAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;

abstract class AbstractAdmin extends SonataAdmin
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \Sonata\AdminBundle\Admin\AbstractAdmin::configureListFields()
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        if ($this->isGranted(Role::SuperAdministrator)) {
            $listMapper->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array()
                )
            ));
        }
    }
}