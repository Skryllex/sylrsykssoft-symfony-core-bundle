/**
 * Utils form animate.css
 */
!function ($) {
    "use strict";
    
    var Animate = {
    	animated : "animated",
    	animationEnd : "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",
    	animationWebkitEnd : "webkitAnimationEnd",
    	animationMozEnd : "mozAnimationEnd",
    	animationMSEnd : "MSAnimationEnd"
    };

    $.fn.extend({
        animateCss: function (animationName) {
        	var cssClass = Animate.animated + ' ' + animationName;
            $(this).addClass(cssClass).one(Animate.animationEnd, function() {
                $(this).removeClass(cssClass);
            });
        }
    });
    
    
    $(document).ready(function() {
        $(".animatedClick").click(function(e){
            var animationName = $(this).attr('data-animation');
            $(this).animateCss(animationName);
        }); 
    });
    
}(window.jQuery);