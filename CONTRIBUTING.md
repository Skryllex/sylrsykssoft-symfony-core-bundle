SylrSyksSoft respects the symfony’s conventions about contributing to the code. So before going further please review the [contributing documentation of Symfony](http://symfony.com/doc/current/contributing/code/patches.html#make-a-pull-request).

## Reporting bugs

If you happen to find a bug, we kindly request you to report it.
You can contact us at the email sylar.sykes@gmail.com

## Pull requests

### Matching coding standards

### Sending a Pull Request

## Contributing to the documentation
