<?php
/**
 * @file
 * Abstract service controller.
 */
namespace SylrSyksSoftSymfony\CoreBundle\Controller;

use Hostnet\Component\Form\Simple\SimpleFormProvider;
use SylrSyksSoftSymfony\CoreBundle\Controller\AbstractController;
use SylrSyksSoftSymfony\CoreBundle\Model\AbstractModel;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Abstract service controller.
 *
 * @package SylrSyksSoftSymfony\CoreBundle\Controller
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *        
 */
abstract class AbstractServiceController extends AbstractController
{

    /**
     *
     * @var string
     */
    protected $bundle_name;

    /**
     *
     * @var string
     */
    protected $class_name;

    /**
     * Default constructor.
     *
     * @param SimpleFormProvider $formProvider
     *            Form provider.
     * @param string $bundle_name
     *            Bundle name.
     * @param string $class_name
     *            Class name.
     */
    public function __construct(SimpleFormProvider $formProvider, $bundle_name, $class_name)
    {
        $this->formProvider = $formProvider;
        $this->bundle_name = strtolower($bundle_name);
        $this->class_name = strtolower($class_name);
    }

    /**
     * Create form.
     *
     * @param Request $request
     *            Request object.
     * @param string $method
     *            Method.
     * @param string $form_action
     *            Form action.
     * @return RedirectResponse|boolean|array|NULL
     */
    public function createCreateForm(Request $request, $method, $form_action)
    {
        $form = $this->get($this->bundle_name . "." . $this->class_name . ".form");
        $handler = $this->get($this->bundle_name . ".form.handler.save." . $this->class_name);
        
        if ($request->isMethod($method)) {
            $success = $this->executeAction($request, $handler, $form);
            
            if ($success) {
                return $success;
            }
        }
        
        return array(
            'form' => $form->createView(),
            'action' => $this->generateUrl($form_action)
        );
    }

    /**
     * Update form.
     *
     * @param Request $request
     *            Request object.
     * @param AbstractModel $object
     *            Object.
     * @param string $method
     *            Method.
     * @param string $form_action
     *            Form action.
     * @return RedirectResponse|boolean|array|NULL
     */
    public function createUpdateForm(Request $request, AbstractModel $object, $method, $form_action)
    {
        $form = $this->get($this->bundle_name . "." . $this->class_name . ".form");
        $handler = $this->get($this->bundle_name . ".form.handler.save." . $this->class_name);
        $form->setData($object);
        
        if ($request->isMethod($method)) {
            $success = $this->executeAction($request, $handler, $form);
            
            if ($success) {
                return $success;
            }
        }
        
        return array(
            'form' => $form->createView(),
            'action' => $this->generateUrl($form_action, array(
                "id" => $object->getId()
            ))
        );
    }

    /**
     * Delete form.
     *
     * @param Request $request
     *            Request object.
     * @param AbstractModel $object
     *            Object.
     * @param string $method
     *            Method.
     * @param string $form_action
     *            Form action.
     * @return RedirectResponse|boolean|array|NULL
     */
    public function createDeleteForm(Request $request, AbstractModel $object, $method, $property, $form_action)
    {
        $form = $this->get($this->bundle_name . "." . $this->class_name . ".delete.form");
        $handler = $this->get($this->bundle_name . ".form.handler.delete." . $this->class_name);
        $form->setData($object);
        
        if ($request->isMethod($method)) {
            $success = $this->executeAction($request, $handler, $form);
            
            if ($success) {
                return $success;
            }
        }
        
        $class_name = get_class($object);
        
        $reflectClass = new \ReflectionClass($class_name);
        $reflectMethod = $reflectClass->getMethod('get' . ucfirst($property));
        $r = "";
        if (null !== $reflectMethod) {
            $r = $reflectMethod->invoke($object);
        }
        
        return array(
            'form' => $form->createView(),
            'action' => $this->generateUrl($form_action, array(
                "id" => $object->getId()
            )),
            $property => $r,
            'delete' => TRUE
        );
    }
}