<?php
/**
 * @file
 * Abstract controller.
 */
namespace SylrSyksSoftSymfony\CoreBundle\Controller;

use Hostnet\Component\Form\AbstractFormHandler;
use Hostnet\Component\Form\Simple\SimpleFormProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Abstract controller.
 *
 * @package SylrSyksSoftSymfony\CoreBundle\Controller
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *        
 */
abstract class AbstractController extends Controller
{

    /**
     *
     * @var SimpleFormProvider
     */
    protected $formProvider;

    /**
     * Default constructor.
     *
     * @param SimpleFormProvider $formProvider            
     */
    public function __construct(SimpleFormProvider $formProvider)
    {
        $this->formProvider = $formProvider;
    }

    /**
     * Get manager.
     *
     * @param string $name
     *            Name.
     * @return \Doctrine\Common\Persistence\ObjectManager
     */
    public function getManager($name = NULL)
    {
        return $this->getDoctrine()->getManager($name);
    }

    /**
     * Get Repository.
     *
     * @param string $className
     *            Class name.
     * @param string $name
     *            Manager name.
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepository($className, $name = NULL)
    {
        $manager = $this->getManager($name);
        
        return $manager->getRepository($className);
    }

    /**
     * Save data.
     *
     * @param Request $request
     *            Request object.
     * @param AbstractFormHandler $handler
     *            Handler action.
     * @param FormInterface $form
     *            Form object.
     * @return RedirectResponse|boolean
     */
    protected function executeAction(Request $request, AbstractFormHandler $handler, FormInterface $form = NULL)
    {
        if (($response = $this->formProvider->handle($request, $handler, $form)) instanceof RedirectResponse) {
            return $response;
        }
        
        return FALSE;
    }
}