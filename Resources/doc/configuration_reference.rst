SylrSyksSoftSymfonyCoreBundle Configuration Reference
=====================================================

All available configuration options are listed below with their default values.

.. code-block:: yaml

    sylr_syks_soft_symfony_core:
        db_driver:       ~ # required
        service:
            base_builder: sylr_syks_soft_symfony_core.builder.object.default
            base_manager: sylr_syks_soft_symfony_core.manager.object.default
