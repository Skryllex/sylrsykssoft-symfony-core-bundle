<?php
/**
 * @file
 * Common master table admin.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use SylrSyksSoftSymfony\Symfony\Component\Admin\AbstractAdmin;
use SylrSyksSoftSymfony\Symfony\Component\Admin\AbstractMasterTableModel;
use Symfony\Component\Validator\Constraints as Assert;

abstract class AbstractMasterTableAdmin extends AbstractAdmin
{

    /**
     * Base pattern.
     *
     * @var string
     */
    const BASE_ROUTE_PATTERN = 'master-tables';

    /**
     * Default datagrid values.
     *
     * @var array
     */
    protected $datagridValues = array(
        '_sort_by' => 'title',
        '_sort_order' => 'ASC'
    );

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\AdminBundle\Admin\Admin::configureDatagridFilters($filter)
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\AdminBundle\Admin\Admin::configureListFields($list)
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title')->add('description');
        parent::configureListFields($listMapper);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\AdminBundle\Admin\Admin::configureShowFields($show)
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('code')
            ->add('title')
            ->add('description', 'textarea');
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\AdminBundle\Admin\Admin::configureFormFields($form)
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('code', 'text', array(
            'constraints' => array(
                new Assert\NotBlank(),
                new Assert\Length(array(
                    'min' => 3,
                    'max' => 50
                ))
            )
        ))
            ->add('title', 'text', array(
            'constraints' => array(
                new Assert\NotBlank(),
                new Assert\Length(array(
                    'max' => 255
                ))
            )
        ))
            ->add('description', 'textarea', array(
            'required' => FALSE,
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 10000
                ))
            )
        ));
    }

    // /**
    // * Non-PHPdoc.
    // *
    // * @see \Sonata\AdminBundle\Admin\Admin::validate($errorElement, $object)
    // */
    // public function validate(ErrorElement $errorElement, $object)
    // {
    // $exists_code = $this->modelManager->findOneBy($this->getClass(), array(
    // 'code' => $object->getCode()
    // ));

    // if (isset($exists_code)) {
    // $errorElement->with('code')
    // ->addViolation('The code value already exist.')
    // ->end();
    // }

    // $exists_title = $this->modelManager->findOneBy($this->getClass(), array(
    // 'title' => $object->getTitle()
    // ));

    // if (isset($exists_title)) {
    // $errorElement->with('title')
    // ->addViolation('The title value already exist.')
    // ->end();
    // }
    // }

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\AdminBundle\Admin\Admin::toString()
     */
    public function toString($object)
    {
        return $object instanceof AbstractMasterTableModel ? $object->getTitle() : 'Master table';
    }
}