SylrSyksSoftSymfonyCoreBundle Tests
===================================

The ``SylrSyksSoftSymfonyCoreBundle`` provides a number of common features for other projects.

Prerequisites
-------------

To run the tests have set the following files:

- phpunit.xml.dist `Configure file <https://phpunit.de/manual/current/en/appendixes.configuration.html>`
- configure autoload in composer.json with::
    "autoload-dev" : {
      "psr-4" : {
         "SylrSyksSoftSymfony\\CoreBundle\\Tests\\" : "Tests/"
      }
   },
- configure bootstrap.php file ``Tests/tests/bootstrap.php``
- configure autoload.php file ``Tests/tests/bootstrap.php.dist``

Execute tests
-------------

Builder
~~~~~~~

- phpunit Tests/Builder/DocumentBuilderTest.php
- phpunit Tests/Builder/EntityBuilderTest.php
- phpunit Tests/Builder/MasterTableBuilderTest.php

Model
~~~~~

- phpunit Tests/Model/DocumentManagerTest.php
- phpunit Tests/Model/EntityManagerTest.php

