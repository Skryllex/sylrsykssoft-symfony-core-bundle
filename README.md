SylrSyksSoft Core Bundle
==================

License
-------

This bundle is available under the [MIT license](Resources/meta/LICENSE).

Filename too long in git for windows:

git config --system core.longpaths true

composer install --prefer-source
